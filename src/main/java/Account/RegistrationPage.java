package Account;

import Utils.GenerateEmail;
import Utils.MyWaits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Home on 12.10.2017.
 */
public class RegistrationPage {
  private final WebDriver driver;
  public RegistrationPage(WebDriver driver) {
    this.driver = driver;
  }
  @FindBy(id = "billinginfo3-form-fullname")
  WebElement namefield;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_countryiso2_chosen']/a")
  WebElement countryfield;

  @FindBy(className = "chosen-search")
  WebElement countryinput;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_phone_code_chosen']/a/div")
  WebElement dropdownphone;

  @FindBy(xpath =".//*[@id='billinginfo3_form_phone_code_chosen']/div/div/input")
  WebElement phoneinput;

  @FindBy(id = "billinginfo3-form-phone")
  WebElement phonesecondfield;

  @FindBy(id = "billinginfo3-form-postalcode")
  WebElement zipcodefield;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_stateiso2_chosen']/a/div")
  WebElement statefield;

  @FindBy(id = "billinginfo3-form-cityname")
  WebElement cityfield;

  @FindBy(xpath=".//*[contains(@class,'signin-switch-account')]")
  WebElement swithaccount;

  @FindBy(xpath = ".//*[contains(@id,'signin3-form-email')]")
  WebElement emailfield;

  @FindBy(id ="signin3-new-customer")
  WebElement continuebuton;

  @FindBy(id ="billinginfo3-form-submit")
  WebElement submitbutton;


  public void registerAccount(String name, String  country, String city, String phone1, String phone2, String zipcode, String state) {
    GenerateEmail generateEmail =new GenerateEmail();
    MyWaits myWaits = new MyWaits(driver);
    myWaits.waitEmail();
    if (swithaccount.isDisplayed()) {
      swithaccount.click();
    }
    emailfield.sendKeys(generateEmail.generateEmail());
    emailfield.submit();
    continuebuton.click();
    myWaits.waitregForm();
    namefield.sendKeys(name);
    namefield.click();
    countryfield.click();
    countryinput.sendKeys(country);
    countryinput.click();
    dropdownphone.click();
    phoneinput.sendKeys(phone1);
    phoneinput.click();
    phonesecondfield.sendKeys(phone2);
    phonesecondfield.click();
    zipcodefield.sendKeys(zipcode);
    zipcodefield.click();
    if (countryfield.getText()=="United States"){
    statefield.sendKeys(state);
    statefield.click();
    }
    cityfield.sendKeys(city);
    submitbutton.click();
}}
