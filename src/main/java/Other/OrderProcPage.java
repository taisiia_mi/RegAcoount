package Other;

import Utils.MyWaits;
import Utils.USUser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Home on 12.10.2017.
 */
public class OrderProcPage {

  private final WebDriver driver;

  public OrderProcPage(WebDriver driver) {
    this.driver = driver;
  }

  @FindBy(xpath=".//*[@id='products']/li[1]")
  WebElement product;

  //@FindBy(xpath = (".//button[contains(@id,'preview-add-to-cart')]"))
  @FindBy(xpath = (".//*[contains(@class,'add-to-cart js-button')]"))
  WebElement addtocart;

  @FindBy(id="cart-summary-checkout")
  WebElement checkout;

  public void chekout() {
    MyWaits myWaits = new MyWaits(driver);
    USUser USUser =new USUser();
    myWaits.waitProduct();
    product.click();
    myWaits.waitAddtoCart();
    addtocart.click();
   // driver.get("https://www.templatemonster.com/cart.php");
    myWaits.waitPopup();
   // driver.findElement(By.className("modal-header")).click();
//    List<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
//    System.out.println(tabs2);
//    driver.switchTo().window(tabs2.get(1));
   myWaits.waitCheckout();
    checkout.click();
  }
}